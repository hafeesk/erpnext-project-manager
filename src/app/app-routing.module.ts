import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AuthGuard } from './common/guards/auth-guard/auth.guard.service';
import { HomeComponent } from './shared-ui/home/home.component';
import { DashboardComponent } from './shared-ui/dashboard/dashboard.component';
import { SettingsComponent } from './project-manager/settings/settings.component';
import { MainMilestonesComponent } from './project-manager/main-milestones/main-milestones.component';
import { MilestonesComponent } from './project-manager/milestones/milestones.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'Project/milestones/:id', component: MilestonesComponent },

  { path: 'Project/main_milestones/:id', component: MainMilestonesComponent },

  {
    path: 'list/:model',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
